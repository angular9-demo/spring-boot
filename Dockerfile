FROM openjdk:8-jdk-alpine

ADD target/angular9-demo-0.0.1-SNAPSHOT.jar app.jar

ENTRYPOINT ["java", "-jar", "app.jar"]

EXPOSE 8081

# Commands
# Export project as jar
# mvn package

# Build
# docker build -t springboot .

# Run
# docker run -p 8081:8081 springboot