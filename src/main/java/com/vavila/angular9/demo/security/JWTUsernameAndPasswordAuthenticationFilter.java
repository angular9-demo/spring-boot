package com.vavila.angular9.demo.security;

import java.io.IOException;
import java.sql.Date;
import java.time.LocalDate;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vavila.angular9.demo.jwt.JWTConfig;
import com.vavila.angular9.demo.model.User;

import io.jsonwebtoken.Jwts;


public class JWTUsernameAndPasswordAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

	private final AuthenticationManager authenticationManager;
	private final JWTConfig config;

	public JWTUsernameAndPasswordAuthenticationFilter(
			AuthenticationManager authenticationManager,
			JWTConfig config) {
		
		this.config = config;
		this.authenticationManager = authenticationManager;
	}

	@Override
	public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
			throws AuthenticationException {

		try {
			
			MyUserDetails authRequest = new ObjectMapper()
					.readValue(request.getInputStream(), MyUserDetails.class);

			Authentication authentication = new UsernamePasswordAuthenticationToken(authRequest.getUsername(),
					authRequest.getPassword());
			Authentication authenticate = authenticationManager.authenticate(authentication);

			return authenticate;

		} catch (IOException e) {
			throw new RuntimeException("Invalid Username and Password.");
		}
	}
	
	@Override
	protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain,
			Authentication authResult) throws IOException, ServletException {
		
		MyUserDetails details = (MyUserDetails) authResult.getPrincipal();
		User user = new User();
		user.setId((int)details.getId());
		user.setUsername(details.getUsername());
		user.setFirstName(details.getFirstName());
		user.setLastName(details.getLastName());
		
		String token = Jwts.builder()
				.setSubject(new ObjectMapper().writeValueAsString(user))
				.claim(config.getAuthorizatiesKey(), authResult.getAuthorities())
				.setExpiration(Date.valueOf(LocalDate.now().plusDays(config.getTokenExpirationAfterDays())))
				.signWith(config.getSecretKeyForSigning())
				.compact();
		
		response.setContentType(MediaType.APPLICATION_JSON_VALUE);
		response.getWriter().write("{\"" + config.getAuthorization() + "\":\"Bearer " + token+ "\"}");
	}
}
