package com.vavila.angular9.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.vavila.angular9.demo.model.Post;
import com.vavila.angular9.demo.model.User;
import com.vavila.angular9.demo.service.PostService;
import com.vavila.angular9.demo.service.UserServiceImpl;

@SpringBootApplication
public class Angular9DemoApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(Angular9DemoApplication.class, args);
	}
	
	@Autowired
	private UserServiceImpl service;
	
	@Autowired
	private PostService postS;

	@Override
	public void run(String... args) throws Exception {
		
		User user1 = service.adduser(new User("anna", "pass", "Anna", "Smith"));
		User user2 = service.adduser(new User("john", "pass", "John", "Doe"));
		
		postS.addPost(new Post(0, "This is the post Title 1", "<p>Lorem ipsum dolor sit, amet consectetur adipisicing elit." +
				      " Voluptas magnam tenetur possimus! Iusto quaerat aliquid suscipit praesentium ratione." +
				         "Vel rem tenetur quae soluta ea laboriosam pariatur? Eum maxime ullam aliquam?</p>" +
				       "<p>" +
				         "Lorem ipsum dolor sit, amet consectetur adipisicing elit." +
				     "Voluptas magnam tenetur possimus! Iusto quaerat aliquid suscipit praesentium ratione." +
				     "Vel rem tenetur quae soluta ea laboriosam pariatur? Eum maxime ullam aliquam?" +
				       "</p>", user1
				       ));
		postS.addPost(new Post(0, "This is the post Title 2", "<p>Lorem ipsum dolor sit, amet consectetur adipisicing elit." +
				      " Voluptas magnam tenetur possimus! Iusto quaerat aliquid suscipit praesentium ratione." +
				         "Vel rem tenetur quae soluta ea laboriosam pariatur? Eum maxime ullam aliquam?</p>" +
				       "<p>" +
				         "Lorem ipsum dolor sit, amet consectetur adipisicing elit." +
				     "Voluptas magnam tenetur possimus! Iusto quaerat aliquid suscipit praesentium ratione." +
				     "Vel rem tenetur quae soluta ea laboriosam pariatur? Eum maxime ullam aliquam?" +
				       "</p>", user2
				       ));
	}
	
	
}
