package com.vavila.angular9.demo.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.vavila.angular9.demo.model.Post;

@Service
public class PostService {

	public static List<Post> POSTS = new ArrayList<>();
	
	public int addPost(Post post) {
		
		if(post.getId() > 0) {
			Optional<Post> saved = POSTS.stream().filter(p -> p.getId() == post.getId())
					.findFirst();
			if(saved.isPresent()) {
				int index = POSTS.indexOf(saved.get());
				POSTS.set(index, post);
			}
			return post.getId();
		}
		else {
			int id = POSTS.size() + 1;
			post.setId(id);
			POSTS.add(post);
			return id;
		}
	}
	
	public List<Post> getAll() {
		return POSTS;
	}
	
	public Optional<Post> getById(int id) {
		Optional<Post> post = POSTS.stream().filter(p -> p.getId() == id)
				.findFirst();
		return post;
	}

	public boolean deletePost(int postId, int userId) {

		Optional<Post> post = POSTS.stream().filter(p -> p.getId() == postId)
				.findFirst();
		if(post.isPresent()) {
			if(post.get().getAuthor().getId() == userId) {
				POSTS.remove(post.get());
				return true;
			}
		}
		return false;
	}
}
