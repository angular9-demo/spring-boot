package com.vavila.angular9.demo.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.vavila.angular9.demo.model.User;
import com.vavila.angular9.demo.security.MyUserDetails;

@Service
public class UserServiceImpl implements UserDetailsService{

	@Autowired
	private PasswordEncoder passwordEncoder;
	
	public static List<MyUserDetails> USERS = new ArrayList<>();
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

		Optional<MyUserDetails> extract = USERS.stream()
				.filter(u -> u.getUsername().equals(username))
				.findFirst();
		
		if(extract.isPresent()) {
			MyUserDetails user = extract.get();
			user.setAuthorities(Arrays.asList(new SimpleGrantedAuthority("AUTHOR")));
			return user;
		}
		throw new UsernameNotFoundException(
				String.format("User with username '%s' is not found.", username));
	}

	public List<MyUserDetails> getUsers() {
		
		return USERS;
	}

	public User adduser(User user) {
		int id = USERS.size() + 1;
		user.setId(id);
		USERS.add(new MyUserDetails(id, user.getUsername(), 
				passwordEncoder.encode(user.getPassword()),
				user.getFirstName(), user.getLastName()));
		return user;
	}
}
