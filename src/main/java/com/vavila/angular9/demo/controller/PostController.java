package com.vavila.angular9.demo.controller;

import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vavila.angular9.demo.jwt.JWTConfig;
import com.vavila.angular9.demo.model.Post;
import com.vavila.angular9.demo.model.User;
import com.vavila.angular9.demo.service.PostService;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;

@RestController
@RequestMapping("/posts")
public class PostController {

	@Autowired
	private PostService service;
	
	@Autowired
	private JWTConfig config;
	
	@GetMapping
	public ResponseEntity<List<Post>> getAllPost() {
		
		return ResponseEntity.ok().body(service.getAll());
	}
	
	@PostMapping
	public ResponseEntity<Object> savePost(@RequestBody Post post,
			@RequestHeader("Authorization") String token) {
		
		User user = getUserFromToken(token);
		if(user != null) {
			if(user.getId() == post.getAuthor().getId()) {
				int id = service.addPost(post);
				return ResponseEntity.ok().body(
						"{\"message\":\"Post updated\", \"id\":"+ id +"}");
			}
		}
		
		return new ResponseEntity<>("{\"message\":\"Saving post failed.\"}", 
				HttpStatus.UNAUTHORIZED);
	}
	
	

	@DeleteMapping("/{id}")
	public ResponseEntity<Object> deletePost(@PathVariable("id") int id,
			@RequestHeader("Authorization") String token) {
		
		User user = getUserFromToken(token);
		if(user != null) {
			if(service.deletePost(id, user.getId())) {
				return ResponseEntity.ok().body("{\"message\":\"Post deleted successfully.\"}");
			}
		}
		
		return new ResponseEntity<>("{\"message\":\"Deletion failed.\"}", 
				HttpStatus.UNAUTHORIZED);
		
	}
	
	
	@GetMapping("/{id}")
	public ResponseEntity<Object> getPostById(@PathVariable("id") int id) {
		
		Optional<Post> post = service.getById(id);
		if(post.isPresent())
			return ResponseEntity.ok().body(post.get());
		else
			return new ResponseEntity<>("{\"message\":\"Post not found\"}", 
					HttpStatus.NOT_FOUND);
	}
	
	
	private User getUserFromToken(String token) {
		
		User user = null;
		try {
			token = token.replace(config.getTokenPrefix(), "");
			Jws<Claims> claimsJws = Jwts.parser()
					.setSigningKey(config.getSecretKeyForSigning())
					.parseClaimsJws(token);
					
			Claims body = claimsJws.getBody();
			byte[] bytes = body.getSubject().getBytes("UTF-8");
			user = new ObjectMapper().readValue(bytes, User.class);
			
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		return user;
	}
}
