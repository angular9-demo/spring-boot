package com.vavila.angular9.demo.controller;

import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.vavila.angular9.demo.model.User;
import com.vavila.angular9.demo.security.MyUserDetails;
import com.vavila.angular9.demo.service.UserServiceImpl;

@RestController
@RequestMapping("/users")
public class UserController {

	
	@Autowired
	private UserServiceImpl service;
	
	@PostMapping("/signup")
	public ResponseEntity<Object> signup(@RequestBody User user) {
		
		Optional<MyUserDetails> extract = service.getUsers().stream()
				.filter(u -> u.getUsername().equals(user.getUsername()))
				.findFirst();
		if(extract.isPresent())
			return new ResponseEntity<>("{\"message\":\"Username already exist.\"}", HttpStatus.UNAUTHORIZED);
		else {
			service.adduser(user);
			return ResponseEntity.ok().body("{\"message\":\"User successfully created.\"}");
		}
			
	}
}
